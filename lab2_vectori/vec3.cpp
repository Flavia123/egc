#include "vec3.h"

namespace egc {
	vec3& vec3 :: operator =(const vec3& srcVector) {
		this->x = srcVector.x;
		this->y = srcVector.y;
		this->z = srcVector.z; 

		return *this;
	}

	vec3 vec3 :: operator +(const vec3& srcVector) const {
		vec3 result;

		result.x = this->x + srcVector.x;
		result.y = this->y + srcVector.y;
		result.z = this->z + srcVector.z;

		return result;
	}

	vec3& vec3 :: operator +=(const vec3& srcVector) {
		return *this = *this + srcVector;
	}

	vec3 vec3 :: operator *(float scalarValue) const {
		vec3 result;
		result.x = this->x * scalarValue;
		result.y = this->y * scalarValue;
		result.z = this->z * scalarValue;
		return result;
	}
}