#include "vec2.h"

namespace egc {

	//v1 = v2
	vec2& vec2 :: operator =(const vec2& srcVector) {
		this->x = srcVector.x;
		this->y = srcVector.y;

		return *this;

		//return *this = vec2(srcVector);

	}
	//v1+v2
	vec2 vec2::operator +(const vec2& srcVector) const {

		vec2 result;

		result.x = this->x + srcVector.x;
		result.y = this->y + srcVector.y;

		return result;

		//return { this->x + srcVector.x, this->y + srcVector.y };
	}

	//v1 += v2 reprezinta v1=v1+v2
	vec2& vec2 :: operator +=(const vec2& srcVector) {
		return *this = *this + srcVector;
	}

	vec2 vec2::operator *(float scalarValue) const {
		vec2 result;

		result.x = this->x * scalarValue;
		result.y = this->y * scalarValue;

		return result;
	}

	vec2 vec2::operator /(float scalarValue) const {
		vec2 result;

		result.x = this->x / scalarValue;
		result.y = this->y / scalarValue;

		return result;
	}

	vec2 vec2 :: operator -(const vec2& srcVector) const {
		vec2 result;

		result.x = this->x - srcVector.x;
		result.y = this->y - srcVector.y;

		return result;
	}

	vec2& vec2 :: operator -=(const vec2& srcVector) {
		return *this = *this - srcVector;
	}

	float vec2 :: length() const {
		return sqrt(this->x * this-> x + this->y * this->y);
	}

	vec2& vec2:: normalize() {
		float lungime = length();
		vec2 result;
		result.x = this->x / lungime;
		result.y = this->y / lungime;
		return result;
	}

	float dotProduct(const vec2& v1, const vec2& v2) {
		return v1.x * v2.x + v1.y * v2.y;
	}
}