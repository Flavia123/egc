#include "mat4.h"

namespace egc {
	mat4& mat4::operator =(const mat4& srcMatrix) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				this->at(i, j) = srcMatrix.at(i, j);
			}
		}
		return *this;
	}

	mat4 mat4::operator *(float scalarValue) const {
		
	}
}