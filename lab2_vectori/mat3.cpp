#include "mat3.h"

namespace egc {
	mat3& mat3::operator =(const mat3& srcMatrix) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				this->at(i, j) = srcMatrix.at(i, j);
			}
		}
		return *this;
	}

	float& mat3::at(int i, int j) {
		return this->matrixData[i + 3 * j];
	}
	//i + 3*j pt ca
	// matrixData: a b c d e f g h i .  aici b e elementul 1. e e elementul 4
	// a d g  // aici b e pe pozitia 1,0. deci il punem pe i+3j 1+0 = 1
	// b e h  // e trebe sa fie pe pozitia 1,1. i+3j . 1 + 3 = 4
	// c g i

	const float& mat3::at(int i, int j) const {
		return this->matrixData[i + 3 * j];
	}

	mat3 mat3::operator +(const mat3& srcMatrix) const {
		mat3 mat = mat3();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				mat.at(i, j) = this->at(i, j) + srcMatrix.at(i, j);
			}
		}
		return mat;
	}

	mat3 mat3::operator *(float scalarValue) const {
		mat3 mat = mat3();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				mat.at(i, j) = this->at(i, j) * scalarValue;
			}
		}
		return mat;
	}


	mat3 mat3::operator *(const mat3& srcMatrix) const {
		mat3 mat = mat3();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				mat.at(i, j) = this->at(i, 0) * srcMatrix.at(0, j) + this->at(i, 1) * srcMatrix.at(1, j) + this->at(i, 2) * srcMatrix.at(2, j);
			}
		}
		return mat;
	}

	vec3 mat3::operator *(const vec3& srcVector) const {
		vec3 vec = vec3();
		vec.x = this->at(0, 0) * srcVector.x + this->at(0, 1) * srcVector.y + this->at(0, 2) * srcVector.z;
		vec.y = this->at(1, 0) * srcVector.x + this->at(1, 1) * srcVector.y + this->at(1, 2) * srcVector.z;
		vec.z = this->at(2, 0) * srcVector.x + this->at(2, 1) * srcVector.y + this->at(2, 2) * srcVector.z;
		return vec;
	}

	float mat3::determinant() const {
		float det;
		det = (this->at(0, 0) * this->at(1, 1) * this->at(2, 2)) + (this->at(0, 1) * this->at(1, 2) * this->at(2, 0)) + (this->at(1, 1) * this->at(0, 2) * this->at(2, 1))
			- ((this->at(2, 0) * this->at(1, 1) * this->at(0, 2) + this->at(0, 0) * this->at(1, 2) * this->at(2, 1) + this->at(1, 1) * this->at(0, 1) * this->at(2, 2)));
		return det;
	}
}
